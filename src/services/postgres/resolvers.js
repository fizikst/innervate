import moment from 'moment';
import crypto from 'crypto';
import { Client } from 'pg';
import configAPI from 'config';

import {oncePerServices, missingService} from '../../common/services/index';

function apolloToRelayResolverAdapter(oldResolver) {
  return function (obj, args, context) {
    return oldResolver(args, context.request);
  }
}

export default oncePerServices(function (services) {
    
  const {
      postgres = missingService('postgres')
  } = services;

  // не смог подключиться к базе через PGConnector конектор внутри resolver функции
  // поэтому работу с базой сделал через стандартный pg
  // дальше я указал где postgres._pool работет корректно а где возвращает ошибку

  function getUsersList(builderContext) {

    // postgres._pool - тут работает корректно

    return async function(obj, args, context) {

      // postgres._pool - тут возвращает ошибку "pool is draining and cannot accept work"

      const { manager, blocked, name, login } = args;
      const keys = [];
      const values = [];
      let i = 0;

      const client = new Client(configAPI.get('postgres'));
      await client.connect();

      if (args.hasOwnProperty('manager')) {
        keys.push(`manager = $${++i}`)
        values.push(manager);
      }
      if (args.hasOwnProperty('blocked')) {
        keys.push(`blocked = $${++i}`)
        values.push(blocked);
      }
      if (args.hasOwnProperty('name')) {
        keys.push(`name LIKE $${++i}`)
        values.push(['%','%'].join(name));
      }
      if (args.hasOwnProperty('login')) {
        keys.push(`login LIKE $${++i}`)
        values.push(['%','%'].join(login));
      }      

      const where = keys.length > 0 ? 'WHERE ' + keys.join(' AND ') : '';
      const { rows } = await client.query(`SELECT * FROM users ${where}`, values);

      await client.end();
      return rows;
    }
  }

  const auth = async (params, req) => {
    const { login, password } = params;
    let response = 'User not found';

    const client = new Client(configAPI.get('postgres'));
    await client.connect();

    const { rows } = await client.query(`SELECT * FROM users WHERE login = $1 AND password_hash = $2 LIMIT 1`, [login, crypto.createHash('md5').update(password).digest("hex")]);
    if (rows.length > 0) {
      response = 'success';
    }

    await client.end();
    return response;
  }

  function signin(builderContext) {
    return async function(obj, args, context) {
      return {
        auth
      };
    }
  }

  return {
    getUsersList,
    signin,
  }
});
