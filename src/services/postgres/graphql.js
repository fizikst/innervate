import {oncePerServices, missingService} from '../../common/services/index'

const PREFIX = '';

export default oncePerServices(function (services) {
  
  const graphqlBuilderSchema = require('../../common/graphql/LevelBuilder.schema');
  
  const resolvers = require('./resolvers').default(services);

  return async function builder(args) {
    
    graphqlBuilderSchema.build_options(args);
    const { parentLevelBuilder, typeDefs, builderContext } = args;
    
    typeDefs.push(`
      type Birthday {
        birthday: String
      }

      type ${PREFIX}userElement {        
        user_id: Int,
        login: String,
        name: String,
        email: String,
        manager: Boolean,
        blocked: Boolean,
        data: Birthday
      }      
    `);
    
    parentLevelBuilder.addQuery({
      name: `users`,
      type: `[${PREFIX}userElement]`,
      args: `
        manager: Boolean,
        blocked: Boolean,
        name: String,
        login: String,
      `,
      resolver: resolvers.getUsersList(builderContext),
    });

    typeDefs.push(`
      type ${PREFIX}Mutation {
        auth(login: String, password: String): String
      }
    `);

    parentLevelBuilder.addMutation({
      name: `signin`,
      type: `${PREFIX}Mutation`,
      resolver: resolvers.signin(builderContext),
    });
  }
});
